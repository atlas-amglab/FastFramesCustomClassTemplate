#include "MyCustomFrame/MyCustomFrame.h"

#ifdef __CINT__

#pragma extra_include "MyCustomFrame/MyCustomFrame.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class MyCustomFrame+;

#endif
